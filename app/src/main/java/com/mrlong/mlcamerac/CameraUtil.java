package com.mrlong.mlcamerac;

import android.content.Context;
import android.content.Intent;

/**
 * @author MrLong
 * @version 1.0
 * @date 2020/11/17 下午 01:35
 */
public class CameraUtil {
    public static final int CAMERA_0 = 0;
    public static final int CAMERA_1 = 1;
    //1.常用变量：
    //1.1录像界面显示方式：
    public static final int UI_SHOW_HIDE = 1;            //隐藏
    public static final int UI_SHOW_CARFRONT_FULL = 2;    //车前界面
    public static final int UI_SHOW_CARINNER_FULL = 3;    //车内界面

    //1.2广播
    public static final String XCAM_ACTION = "com.lhs.xcam.cmdaction";
    public final static String XCAM_NOTIFY_ACTION = "com.lhs.camera.state";
    public static final String SYS_ACTION = "com.lhs.xsetting.action";

    //2.常用接口：
    //2.1打开摄像头并开始录像
//    public void startRecord(Context context) {
//        Intent nn = new Intent(XCAM_ACTION);
//        nn.putExtra("cmd", "start");
//        nn.putExtra("uistyle", UI_SHOW_CARFRONT_FULL);
//        nn.setPackage("cn.alauncher.dvr");
//        context.sendBroadcast(nn);
//    }
//    public static final String XCAM_ACTION = "com.lhs.xcam.cmdaction";
    public static final String APK_PKNAME = "cn.alauncher.dvr";

    //1.1开始录制
    public static void startRecord(Context context) {
        Intent nn = new Intent(XCAM_ACTION);
        nn.putExtra("cmd", "start");
        nn.setPackage(APK_PKNAME);
        context.sendBroadcast(nn);
    }

    //1.2停止录制
    public static void stopRecord(Context context) {
        Intent nn = new Intent(XCAM_ACTION);
        nn.putExtra("cmd", "stop");
        nn.setPackage(APK_PKNAME);
        context.sendBroadcast(nn);
    }

    //2.2开始推流：
    public static void startStreamPush(Context context, String url, int cameraType) {
        Intent nn = new Intent(XCAM_ACTION);
        nn.putExtra("cmd", "startrtc");
        nn.putExtra("url", url);
        nn.putExtra("camtype", cameraType);//0 车内， 1 为车前
        nn.setPackage("cn.alauncher.dvr");
        context.sendBroadcast(nn);
    }

    //2.3结束推流
    public static void stopStreamPush(Context context, int cameraType) {

        Intent nn = new Intent(XCAM_ACTION);
        nn.putExtra("cmd", "stoprtc");
        nn.putExtra("camtype", cameraType);//0 车内， 1 为车前
        nn.setPackage("cn.alauncher.dvr");
        context.sendBroadcast(nn);
    }

    //2.4摄像头状态（系统发出的广播）
    public void notifyCameraState(Context context, int state, boolean bFront) {
        Intent nn = new Intent(XCAM_NOTIFY_ACTION);
        nn.putExtra("cmd", "camstate");
        nn.putExtra("state", state);        //0为摄像头未跑， 1为正在录像，2为在推流，3为推流结束
        nn.putExtra("camtype", bFront ? 0 : 1);    //0为车前摄像头，1 为车内摄像头
        context.sendBroadcast(nn);
    }

    //3.系统接口：
    // 3.1重启系统
    public void rebootSystem(Context context) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "reboot");
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.2关机
    public void closeSystem(Context context) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "shutdown");
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.3唤醒系统
    public void wakeupSystem(Context context) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "wakeup");
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.4休眠系统
    public void sleepSystem(Context context) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "sleep");
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.5设置系统灭屏时间
    public void setSystemBackLightTime(Context context, int mills) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "setbglight");
        nn.putExtra("bglighttime", mills);
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.6设置系统时间
    public void setSystemTime(Context context, long mills) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "settime");
        nn.putExtra("timemills", mills);
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }

    //3.7静默安装应用
    public void installApk(Context context, String apkPath) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "install");
        nn.putExtra("pkpath", apkPath);
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }


    //3.8静默卸载应用
    public void uninstallApk(Context context, String packageName) {
        Intent nn = new Intent(SYS_ACTION);
        nn.putExtra("cmd", "uninstall");
        nn.putExtra("pkname", packageName);
        nn.setPackage("com.android.settings");
        context.sendBroadcast(nn);
    }
}
