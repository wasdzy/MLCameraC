package com.mrlong.mlcamerac.push;

public interface PushConnectListenr {

    void onConnecting();

    void onConnectSuccess();

    void onConnectFail(String msg);

}
