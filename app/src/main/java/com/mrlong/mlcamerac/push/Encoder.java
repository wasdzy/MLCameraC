package com.mrlong.mlcamerac.push;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.mrlong.mlcamerac.MainActivity;
import com.mrlong.mlcamerac.audioEncoder.MAudioEncoder;
import com.mrlong.mlcamerac.util.NetSpeed;
import com.mrlong.mlcamerac.util.NetSpeedTimer;
import com.mrlong.mlcamerac.videoEncoder.MVideoEncoder;

import java.util.Arrays;

public class Encoder {

    private static String TAG = "Encoder==>";
    private MVideoEncoder mVideoEncoder;
    private MAudioEncoder audioEncoder;
    private RtmpPusher rtmpPusher;

    private static volatile Encoder instance = null;

    private Encoder() {
    }

    /**
     * 放入相机onPreviewFrame数据
     *
     * @param data
     */
    public void putYUVData(byte[] data) {
        if (data.length > 0 && null != mVideoEncoder)
            mVideoEncoder.putYUVData(data);
    }

    public static Encoder getInstance() {
        if (null == instance) {
            synchronized (Encoder.class) {
                if (null == instance) {
                    instance = new Encoder();
                }
            }
        }
        return instance;
    }

    public void init(Context context, String pushUrl, int width, int height, int framerate, final int biterate, PushConnectListenr pushConnectListenr) {
        NetSpeedTimer mNetSpeedTimer = new NetSpeedTimer(context, new NetSpeed(), new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == NetSpeedTimer.NET_SPEED_TIMER_DEFAULT) {
                    String speed = (String) msg.obj;
                    //打印你所需要的网速值，单位默认为kb/s
                    int spInt = Integer.valueOf(speed);
//                    if (spInt < 6) {
//                        mVideoEncoder.changeBit(MainActivity.biterate);
//                    } else if (12 > spInt && spInt >= 6) {
//                        mVideoEncoder.changeBit(MainActivity.biterate * 5);
//                    } else {
//                        mVideoEncoder.changeBit(MainActivity.biterate * 10);
//                    }
                    Log.e("===>", "current net speed  = " + speed);
                }
            }
        }).setDelayTime(1000).setPeriodTime(2000);
        //在想要开始执行的地方调用该段代码
        mNetSpeedTimer.startSpeedTimer();
        // 方式一  初始化视频编码器
        mVideoEncoder = new MVideoEncoder(height, width, framerate, biterate);
        mVideoEncoder.setOnMediaInfoListener(new MVideoEncoder.OnMediaInfoListener() {
            @Override
            public void pushVideoData(byte[] data, boolean keyframe) {
                if (rtmpPusher != null) {
                    rtmpPusher.pushVideoData(data, keyframe);
                }
            }

            @Override
            public void pushSPSPPS(byte[] sps, byte[] pps) {

            }
        });

        //初始化音频编码器
        audioEncoder = new MAudioEncoder();
        audioEncoder.setOnMediaInfoListener(new MAudioEncoder.OnMediaInfoListener() {
            @Override
            public void onAudioInfo(byte[] data) {
                if (rtmpPusher != null) {
                    rtmpPusher.pushAudioData(data);
                }
            }
        });
        //初始化推流器
        rtmpPusher = new RtmpPusher();
        if (null == pushUrl) {
            throw new RuntimeException("推流地址不能为空");
        }
        rtmpPusher.initLivePush(pushUrl);
        if (null != pushConnectListenr)
            rtmpPusher.setWlConnectListenr(pushConnectListenr);
    }

    public enum CameraLocation {
        BACK("1"), FRONT("0");

        CameraLocation(String type) {
            this.type = type;
        }

        public void setType(String type) {
            this.type = type;
        }

        private String type;
    }

    public Encoder setCamera(String cameraLocation) {
        mVideoEncoder.setFrontOrBack(cameraLocation);
        return this;
    }

    public void start() {
        if (null == mVideoEncoder || null == audioEncoder) {
            throw new RuntimeException("请先调用init");
        }
        mVideoEncoder.startEncoderThread();
        audioEncoder.startRecord();
    }

    public void stop() {
        if (null != rtmpPusher) {
            rtmpPusher.stopPush();
        }
        if (null != mVideoEncoder) {
            mVideoEncoder.stopThread();
        }
        if (null != audioEncoder) {
            audioEncoder.stopRecord();
        }
    }
}
